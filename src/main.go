package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	//"regexp"
	"strconv"
	"strings"
	"unicode"
)

const (
	script_start = "<script>\n"
	script_end   = "\n</script>"
	body_start   = "<main>\n"
	body_end     = "\n</main>"
)

func main() {
	file, err := os.Create("./routes/index.svelte")

	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	script := `	import Counter from '$components/Counter.svelte';`

	body := `	<h1>Hello world!</h1>

	<Counter/>
  <p>わたしわなまえシンデレです。</p>


<h1>Some title</h1>
<h2>Some title</h2>
<h3>Some title</h3>
<h4>Some title</h4>
<h5>Some title</h5>
<h6>Some title</h6>

<ul>
	<li>Item</li>
	<li>Item</li>
	<li>Item</li>
</ul>

<ol>
	<li>Item</li>
	<li>Item</li>
	<li>Item</li>
</ol>

<ol>
	<li>Item</li>
	<li>
		<ul>
			<li>Item</li>
			<li>Item</li>
		</ul>
	</li>
	<li><pre>Item
	More of same Item</pre></li>
</ol>

<p>Some random paragraph</p>
`

	body = fmt.Sprintf("%s\n%s", body, buildBody(parseFile()))

	n, err := fmt.Fprint(file, script_start, script, script_end, body_start, body, body_end, style)

	if err != nil {

		log.Fatal(err)
	}

	fmt.Println(n, "bytes written")
	fmt.Println("done")
}

type token struct {
	id  int
	val string
}

func buildBody(tokens []token) string {
	var builder strings.Builder
	for i := 0; i < len(tokens); i++ {
		switch tokens[i].id {
		case Heading:
			numToken, str := buildHeading(tokens[i:])
			builder.WriteString(str)
			i += numToken
		case Newline:
			builder.WriteString("<br>")
		case TOC:
			str := buildTOC(tokens)
			builder.WriteString(str)
		case Italic:
			fallthrough
		case UL:
			numToken, str := buildUL(tokens[i:])
			builder.WriteString(str)
			i += numToken
		case Blockquote:
			builder.WriteString("<h1>Blockquote NOT IMPLEMENTED</h1>")
		case OL:
			numToken, str := buildOL(tokens[i:])
			builder.WriteString(str)
			i += numToken
		case CodeBlock:
			builder.WriteString("<h1>CodeBlock NOT IMPLEMENTED</h1>")
		case HR:
			if i+1 < len(tokens) && tokens[i+1].id == Newline {
				i++
				builder.WriteString("<hr>")
				break
			}
			numToken, str := buildText(tokens[i:])
			builder.WriteString(str)
			i += numToken
		default:
			numToken, str := buildText(tokens[i:])
			builder.WriteString(str)
			i += numToken
		}
		builder.WriteRune('\n')
	}
	builder.WriteRune('\n')

	return builder.String()
}

func buildTOC(tokens []token) string {
	var builder strings.Builder
	curLvl := 0
	prevWasNewline := true
	for i := 0; i < len(tokens); i++ {
		switch tokens[i].id {
		case Heading:
			if !prevWasNewline {
				prevWasNewline = false
				break
			}
			lvl, numToken, str := buildTOCHeading(tokens[i:])
			if numToken != 0 {
				for ; lvl > curLvl; curLvl++ {
					builder.WriteString("<ol>")
				}
				for ; lvl < curLvl; curLvl-- {
					builder.WriteString("</ol>")
				}
				builder.WriteRune('\n')
				builder.WriteString(str)
				i += numToken
				builder.WriteRune('\n')
			}
		case Newline:
			prevWasNewline = true
		default:
			prevWasNewline = false
		}
	}
	for ; curLvl > 0; curLvl-- {
		builder.WriteString("</ol>")
	}
	builder.WriteRune('\n')

	return builder.String()
}

func buildText(tokens []token) (int, string) {
	var builder strings.Builder
	i := 0
	builder.WriteString("<p>")
	for ; i < len(tokens); i++ {
		switch tokens[i].id {
		case Newline:
			builder.WriteString("</p>")
			return i, builder.String()
		case Bold:
			numToken, str := buildBold(tokens[i:])
			builder.WriteString(str)
			i += numToken
		case Italic:
			numToken, str := buildItalic(tokens[i:])
			builder.WriteString(str)
			i += numToken
		case Code:
			numToken, str := buildCode(tokens[i:])
			builder.WriteString(str)
			i += numToken
		case SquareOpen:
			numToken, str := buildLink(tokens[i:])
			if numToken != 0 {
				builder.WriteString(str)
				i += numToken
			} else {
				builder.WriteString(tokens[i].val)
			}
		default:
			builder.WriteString(tokens[i].val)
		}
	}
	log.Fatal("KINDA FATALE BOYZ")
	return 0, ""
}

func buildUL(tokens []token) (int, string) {
	var builder strings.Builder
	i := 0
	first := true
	builder.WriteString("<ul>\n")
	for ; i < len(tokens); i++ {
		switch tokens[i].id {
		case Newline:
			builder.WriteString("</ul>")
			return i, builder.String()
		case Italic:
			if !first {
				builder.WriteString("</li>\n")
			}
			builder.WriteString("<li>")
			first = false
		default:
			numToken, str := buildText(tokens[i:])
			builder.WriteString(str)
			i += numToken
		}
	}
	return 0, ""
}

func buildOL(tokens []token) (int, string) {
	var builder strings.Builder
	i := 0
	curID := int64(0)
	builder.WriteString("<ol>\n")
	for ; i < len(tokens); i++ {
		switch tokens[i].id {
		case Newline:
			builder.WriteString("</ol>")
			return i, builder.String()
		case OL:
			OLID, err := strconv.ParseInt(tokens[i].val[:len(tokens[i].val)-1], 10, 32)
			if err != nil || curID+1 != OLID {
				return 0, ""
			}
			if curID != 0 {
				builder.WriteString("</li>\n")
			}
			builder.WriteString("<li>")
			curID++
		default:
			numToken, str := buildText(tokens[i:])
			builder.WriteString(str)
			i += numToken
		}
	}
	return 0, ""
}

func buildLink(tokens []token) (int, string) {
	totalNumToken := 1
	numToken, text := buildLinkText(tokens[totalNumToken:])
	if numToken == 0 || numToken+totalNumToken >= len(tokens) || tokens[numToken+totalNumToken].id != ResourceMid {
		return 0, ""
	}
	totalNumToken += numToken + 1
	numToken, link := buildLinkLink(tokens[totalNumToken:])
	if numToken == 0 || numToken+totalNumToken >= len(tokens) || tokens[numToken+totalNumToken].id != BraceClose {
		return 0, ""
	}
	totalNumToken += numToken
	return totalNumToken, fmt.Sprintf("<a href=\"%s\">%s</a>", link, text)
}

func buildLinkText(tokens []token) (int, string) {
	var builder strings.Builder
	i := 0
	for ; i < len(tokens); i++ {
		switch tokens[i].id {
		case ResourceMid:
			return i, builder.String()
		case SquareClose:
			return 0, ""
		case Bold:
			numToken, str := buildBold(tokens[i:])
			builder.WriteString(str)
			i += numToken
		case Italic:
			numToken, str := buildItalic(tokens[i:])
			builder.WriteString(str)
			i += numToken
		default:
			builder.WriteString(tokens[i].val)
		}
	}
	log.Fatal("KINDA FATALE BOYZ")
	return 0, ""
}

func buildLinkLink(tokens []token) (int, string) {
	var builder strings.Builder
	i := 0
	hadText := false
	for ; i < len(tokens); i++ {
		switch tokens[i].id {
		case Heading:
			builder.WriteString("#")
		case Indent:
			if hadText {
				builder.WriteString("_")
			}
		case BraceClose:
			return i, builder.String()
		default:
			hadText = true
			builder.WriteString(tokens[i].val)
		}
	}
	log.Fatal("KINDA FATALE BOYZ")
	return 0, ""
}

func buildBold(tokens []token) (int, string) {
	var builder strings.Builder
	i := 0
	builder.WriteString("<b>")
	for i++; i < len(tokens); i++ {
		switch tokens[i].id {
		case Indent:
			fallthrough
		case Text:
			builder.WriteString(tokens[i].val)
			break
		case Bold:
			builder.WriteString("</b>")
			return i, builder.String()
		default:
			return 0, ""
		}
	}
	return 0, ""
}

func buildItalic(tokens []token) (int, string) {
	var builder strings.Builder
	i := 0
	builder.WriteString("<em>")
	for i++; i < len(tokens); i++ {
		switch tokens[i].id {
		case Indent:
			fallthrough
		case Text:
			builder.WriteString(tokens[i].val)
			break
		case Italic:
			builder.WriteString("</em>")
			return i, builder.String()
		default:
			return 0, ""
		}
	}
	return 0, ""
}

func buildCode(tokens []token) (int, string) {
	var builder strings.Builder
	i := 0
	builder.WriteString("<code>")
	for i++; i < len(tokens); i++ {
		switch tokens[i].id {
		case Code:
			builder.WriteString("</code>")
			return i, builder.String()
		default:
			builder.WriteString(tokens[i].val)
		}
	}
	return 0, ""
}

func buildHeading(tokens []token) (int, string) {
	var builder strings.Builder
	i := 0
	h := len(tokens[i].val)
	hittText := false
	for i++; i < len(tokens); i++ {
		switch tokens[i].id {
		case Newline:
			s := builder.String()
			return i, fmt.Sprintf("<h%[1]d id=\"%s\">%s</h%[1]d>", h, strings.ReplaceAll(strings.ReplaceAll(s, " ", "_"), "	", "_"), s)
		case Indent:
			if !hittText {
				break
			}
			fallthrough
		default:
			hittText = true
			builder.WriteString(tokens[i].val)
		}
	}
	return 0, ""
}

func buildTOCHeading(tokens []token) (int, int, string) {
	var builder strings.Builder
	i := 0
	h := len(tokens[i].val)
	hittText := false
	for i++; i < len(tokens); i++ {
		switch tokens[i].id {
		case Newline:
			s := builder.String()
			return h, i, fmt.Sprintf("<a href=\"#%s\"><li>%s</li></a>", strings.ReplaceAll(strings.ReplaceAll(s, " ", "_"), "	", "_"), s)
		case Indent:
			if !hittText {
				break
			}
			fallthrough
		default:
			hittText = true
			builder.WriteString(tokens[i].val)
		}
	}
	return 0, 0, ""
}

func parseFile() (tokens []token) {
	file, err := os.Open("/home/sindre/japanese-grammar.md")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	tokens = make([]token, 0)
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		curProg := 0
		for curProg < len(line) {
			t := getToken(line[curProg:])
			if t != nil {
				if curProg != 0 {
					tokens = append(tokens, token{id: Text, val: line[:curProg]})
					line = line[curProg:]
					curProg = 0
				}
				tokens = append(tokens, *t)
				line = line[len(t.val):]
			} else {
				curProg++
			}
		}
		if curProg != 0 {
			tokens = append(tokens, token{id: Text, val: line[:curProg]})
			line = line[curProg:]
			curProg = 0
		}
		tokens = append(tokens, token{id: Newline, val: "\n"})
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return
}

func getToken(s string) *token {
	if strings.HasPrefix(s, "######") {
		return &token{
			id:  Heading,
			val: "######",
		}
	} else if strings.HasPrefix(s, "#####") {
		return &token{
			id:  Heading,
			val: "#####",
		}
	} else if strings.HasPrefix(s, "####") {
		return &token{
			id:  Heading,
			val: "####",
		}
	} else if strings.HasPrefix(s, "###") {
		return &token{
			id:  Heading,
			val: "###",
		}
	} else if strings.HasPrefix(s, "##") {
		return &token{
			id:  Heading,
			val: "##",
		}
	} else if strings.HasPrefix(s, "#") {
		return &token{
			id:  Heading,
			val: "#",
		}
	} else if strings.HasPrefix(s, "\n") { //TODO: REMOVE? READ WHOLE FILE AS INSTED OF LINE?
		return &token{
			id:  Newline,
			val: "\n",
		}
	} else if strings.HasPrefix(s, "	") {
		return &token{
			id: Indent,
			val: "	",
		}
	} else if strings.HasPrefix(s, " ") {
		return &token{
			id:  Indent,
			val: " ",
		}
	} else if strings.HasPrefix(s, "**") {
		return &token{
			id:  Bold,
			val: "**",
		}
	} else if strings.HasPrefix(s, "*") {
		return &token{
			id:  Italic,
			val: "*",
		}
	} else if strings.HasPrefix(s, "---") {
		return &token{
			id:  HR,
			val: "---",
		}
	} else if strings.HasPrefix(s, "-") {
		return &token{
			id:  UL,
			val: "-",
		}
	} else if strings.HasPrefix(s, ">") {
		return &token{
			id:  Blockquote,
			val: ">",
		}
	} else if strings.HasPrefix(s, "```") {
		return &token{
			id:  CodeBlock,
			val: "```",
		}
	} else if strings.HasPrefix(s, "`") {
		return &token{
			id:  Code,
			val: "`",
		}
	} else if strings.HasPrefix(s, "|") {
		return &token{
			id:  TableDivider,
			val: "|",
		}
	} else if strings.HasPrefix(s, "![") {
		return &token{
			id:  ImageBeginning,
			val: "![",
		}
	} else if strings.HasPrefix(s, "[^") {
		return &token{
			id:  FootnoteStart,
			val: "[^",
		}
	} else if strings.HasPrefix(s, "[TOC]") {
		return &token{
			id:  TOC,
			val: "[TOC]",
		}
	} else if strings.HasPrefix(s, "[") {
		return &token{
			id:  SquareOpen,
			val: "[",
		}
	} else if strings.HasPrefix(s, "](") {
		return &token{
			id:  ResourceMid,
			val: "](",
		}
	} else if strings.HasPrefix(s, "]") {
		return &token{
			id:  SquareClose,
			val: "]",
		}
	} else if strings.HasPrefix(s, ")") {
		return &token{
			id:  BraceClose,
			val: ")",
		}
	} else if strings.HasPrefix(s, ": ") {
		return &token{
			id:  Definition,
			val: ": ",
		}
	} else if strings.HasPrefix(s, "~~") {
		return &token{
			id:  Strikethrough,
			val: "~~",
		}
	} else if strings.HasPrefix(s, "- [x]") {
		return &token{
			id:  TaskDone,
			val: "- [x]",
		}
	} else if strings.HasPrefix(s, "- [ ]") {
		return &token{
			id:  Task,
			val: "- [ ]",
		}
	} else if olid := numberAndPeriod(s); olid != "" {
		return &token{
			id:  OL,
			val: olid,
		}
	}
	return nil
}

func numberAndPeriod(s string) string {
	var builder strings.Builder
	for _, v := range s {
		if unicode.IsDigit(v) {
			builder.WriteRune(v)
		} else if v == '.' {
			builder.WriteRune(v)
			break
		} else {
			return ""
		}
	}
	return builder.String()
}

const (
	Text int = iota
	Newline
	Indent
	Heading
	Bold
	Italic
	Blockquote
	OL
	UL
	Code
	CodeBlock
	HR
	ImageBeginning
	SquareOpen
	SquareClose
	BraceClose
	ResourceMid
	TableDivider
	FootnoteStart
	Definition
	Strikethrough
	Task
	TaskDone
	TOC
)

func tokenIdToString(id int) string {
	switch id {
	case Text:
		return "Text"
	case Newline:
		return "Newline"
	case Indent:
		return "Indent"
	case Heading:
		return "Heading"
	case Bold:
		return "Bold"
	case Italic:
		return "Italic"
	case Blockquote:
		return "Blockquote"
	case OL:
		return "OL"
	case UL:
		return "UL"
	case Code:
		return "Code"
	case CodeBlock:
		return "CodeBlock"
	case HR:
		return "HR"
	case ImageBeginning:
		return "ImageBeginning"
	case SquareOpen:
		return "SquareOpen"
	case SquareClose:
		return "SquareClose"
	case BraceClose:
		return "BraceClose"
	case ResourceMid:
		return "ResourceMid"
	case TableDivider:
		return "TableDivider"
	case FootnoteStart:
		return "FootnoteStart"
	case Definition:
		return "Definition"
	case Strikethrough:
		return "Strikethrough"
	case Task:
		return "Task"
	case TaskDone:
		return "TaskDone"
	case TOC:
		return "TOC"
	}
	return ""
}

func encapsulate(tag, s string) string {
	return fmt.Sprintf("<%[1]s>%[2]s</%[1]s>", tag, s)
}

func encapsulateWithId(tag, s string) string {
	return fmt.Sprintf("<%[1]s id=\"%[3]s\">%[2]s</%[1]s>", tag, s, strings.ReplaceAll(s, " ", "_"))
}

func addLink(s string) string {
	texts := strings.Split(strings.ReplaceAll(s[1:], "#", ""), "](")
	log.Println(fmt.Sprintf("<a href=\"%s\">%s</a>", texts[1][1:], texts[0]))
	return fmt.Sprintf("<a href=\"#%s\">%s</a>", strings.ReplaceAll(texts[1][1:], " ", "_"), texts[0])
}

const style = `
<style>
	:root {
		font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
	}
	pre {
	  font-family: inherit !important;
	  padding: 1em !important;
	  white-space: pre-line !important;
	  text-align: left !important;
	}
</style>`
